package net.phoenixgi.greatnothing.inputs;

import net.phoenixgi.greatnothing.engine.input.StandardInput;
import net.phoenixgi.greatnothing.scenes.MiddleWorldScene;
import net.phoenixgi.greatnothing.world.entity.Entity;
import net.phoenixgi.greatnothing.world.entity.EntityState;

import java.awt.event.KeyEvent;

public class MainHeroInput extends StandardInput {
    MiddleWorldScene scene;

    public MainHeroInput(MiddleWorldScene scene) {
        this.scene = scene;
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        Entity hero = scene.getHero();
        EntityState heroState = hero.getState();
        switch (keyEvent.getKeyCode()) {
            case KeyEvent.VK_W:
                heroState.moveUp(true);
                break;
            case KeyEvent.VK_S:
                heroState.moveDown(true);
                break;
            case KeyEvent.VK_A:
                heroState.moveLeft(true);
                break;
            case KeyEvent.VK_D:
                heroState.moveRight(true);
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        Entity hero = scene.getHero();
        EntityState heroState = hero.getState();
        switch (keyEvent.getKeyCode()) {
            case KeyEvent.VK_W:
                heroState.moveUp(false);
                break;
            case KeyEvent.VK_S:
                heroState.moveDown(false);
                break;
            case KeyEvent.VK_A:
                heroState.moveLeft(false);
                break;
            case KeyEvent.VK_D:
                heroState.moveRight(false);
                break;
        }
    }
}