package net.phoenixgi.greatnothing.inputs;

import net.phoenixgi.greatnothing.engine.input.StandardInput;
import net.phoenixgi.greatnothing.scenes.MenuScene;
import net.phoenixgi.greatnothing.scenes.MiddleWorldScene;

import java.awt.event.KeyEvent;

public class MenuInput extends StandardInput {
    MenuScene scene;

    public MenuInput(MenuScene scene) {
        this.scene = scene;
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {
        super.keyTyped(keyEvent);
        System.out.println(keyEvent.getKeyChar());
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        super.keyPressed(keyEvent);
        System.out.println(keyEvent.getKeyChar());
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        super.keyReleased(keyEvent);
        System.out.println(keyEvent.getKeyChar());
        if (keyEvent.getKeyCode() == KeyEvent.VK_ENTER) {
            scene.getGameUpdater().setScene(new MiddleWorldScene(scene.getGameUpdater()));
        }
    }
}
