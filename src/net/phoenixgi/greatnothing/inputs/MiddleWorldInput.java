package net.phoenixgi.greatnothing.inputs;

import net.phoenixgi.greatnothing.engine.input.StandardInput;
import net.phoenixgi.greatnothing.scenes.MiddleWorldScene;

import java.awt.event.KeyEvent;

public class MiddleWorldInput extends StandardInput {
    MiddleWorldScene scene;
    MainHeroInput heroInput;

    public MiddleWorldInput(MiddleWorldScene scene) {
        this.scene = scene;
        this.heroInput = new MainHeroInput(scene);
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        heroInput.keyPressed(keyEvent);
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        heroInput.keyReleased(keyEvent);
    }
}
