package net.phoenixgi.greatnothing.inputs;

import net.phoenixgi.greatnothing.engine.input.StandardInput;
import net.phoenixgi.greatnothing.scenes.TitleScene;

import java.awt.event.KeyEvent;

public class TitleInput extends StandardInput {
    TitleScene scene;

    public TitleInput(TitleScene scene) {
        this.scene = scene;
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        super.keyReleased(keyEvent);
        if (keyEvent.getKeyCode() == KeyEvent.VK_SPACE) {
            scene.setMenuScreen();
        }
    }
}
