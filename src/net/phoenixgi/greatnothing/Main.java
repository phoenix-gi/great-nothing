package net.phoenixgi.greatnothing;

import net.phoenixgi.greatnothing.engine.GameCanvas;
import net.phoenixgi.greatnothing.engine.GameScene;
import net.phoenixgi.greatnothing.engine.GameUpdater;
import net.phoenixgi.greatnothing.engine.GameWindow;
import net.phoenixgi.greatnothing.engine.JPanelGameCanvas;
import net.phoenixgi.greatnothing.scenes.TitleScene;

public class Main {
    public static void main(String[] args) {
        Assets.load();
        GameCanvas canvas = new JPanelGameCanvas();
        GameWindow window = new GameWindow(canvas, "Great Nothing", 800, 600);
        GameUpdater updater = new GameUpdater(window);
        GameScene titleScene = new TitleScene(updater);
        updater.setScene(titleScene);
        updater.start();
    }
}
