package net.phoenixgi.greatnothing;

import net.phoenixgi.greatnothing.world.entity.Sprite;
import net.phoenixgi.greatnothing.world.entity.SpriteAnimation;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Assets {
    public static BufferedImage logo;
    public static BufferedImage greyMageImage;
    public static Sprite greyMage;
    public static SpriteAnimation greyMageStopLeft;
    public static SpriteAnimation greyMageStopRight;
    public static SpriteAnimation greyMageStopDown;
    public static SpriteAnimation greyMageStopUp;

    public static void load() {
        try {
            logo = ImageIO.read(Assets.class.getResource("/assets/logo.png"));
            greyMageImage = ImageIO.read(Assets.class.getResource("/assets/greyMage.png"));
            greyMageStopRight = new SpriteAnimation(greyMageImage.getWidth(), 32, 64, 0, 1);
            greyMageStopLeft = new SpriteAnimation(greyMageImage.getWidth(), 32, 64, 0, 1, 8);
            greyMageStopUp = new SpriteAnimation(greyMageImage.getWidth(), 32, 64, 0, 1, 16);
            greyMageStopDown = new SpriteAnimation(greyMageImage.getWidth(), 32, 64, 0, 1, 24);
            greyMage = new Sprite(greyMageImage, greyMageStopDown);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}