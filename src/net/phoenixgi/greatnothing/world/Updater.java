package net.phoenixgi.greatnothing.world;

import net.phoenixgi.greatnothing.world.entity.Entity;
import net.phoenixgi.greatnothing.world.entity.EntityState;

public class Updater {
    public static void update(World world, double deltaTime) {
        for (Entity e : world.getEntities()) {
            e.getView().getSprite().getSpriteAnimation().update(deltaTime);
            EntityState s = e.getState();
            Vector2 moveVelocity = new Vector2();
            if(s.isMoveUp()) {
                moveVelocity.addThis(0,-1);
            }
            if(s.isMoveDown()) {
                moveVelocity.addThis(0,1);
            }
            if(s.isMoveLeft()) {
                moveVelocity.addThis(-1,0);
            }
            if(s.isMoveRight()) {
                moveVelocity.addThis(1,0);
            }
            moveVelocity = moveVelocity.norm().mul(s.getMoveSpeed());
            s.setPosition(s.getPosition().add(s.getVelocity().add(moveVelocity).mul(deltaTime)));
        }
    }
}
