package net.phoenixgi.greatnothing.world;

import net.phoenixgi.greatnothing.world.entity.Entity;

import java.util.ArrayList;

public class World {
    ArrayList<Entity> entities;

    public World() {
        entities = new ArrayList<>();
    }

    public void addEntity(Entity e) {
        entities.add(e);
    }

    public void removeEntity(Entity e) {
        entities.remove(e);
    }

    public ArrayList<Entity> getEntities() {
        return entities;
    }
}
