package net.phoenixgi.greatnothing.world.entity;

public class SpriteAnimation {
    private double frameDurations[];
    private int[][] frameProps;

    private int currentFrameNum;
    private double timeLeft;

    public SpriteAnimation(double frameDurations[], int[][] frameProps) {
        this.frameDurations = frameDurations;
        this.frameProps = frameProps;
        currentFrameNum = 0;
        timeLeft = 0;
    }

    public SpriteAnimation(int imageWidth, int frameWidth, int frameHeight, double frameTime, int frameCount, int firstShift) {
        currentFrameNum = 0;
        timeLeft = 0;

        int framesInWidth = imageWidth / frameWidth;

        frameDurations = new double[frameCount];
        frameProps = new int[frameCount][];
        for (int frameIndex = 0; frameIndex < frameDurations.length; frameIndex++) {
            frameDurations[frameIndex] = frameTime;
            int widthShift = ((frameIndex + firstShift) % framesInWidth) * frameWidth;
            int heightShift = ((frameIndex + firstShift) / framesInWidth) * frameHeight;
            frameProps[frameIndex] = new int[]{
                    frameWidth,
                    frameHeight,
                    widthShift,
                    heightShift,
                    frameWidth + widthShift,
                    frameHeight + heightShift
            };
        }
    }

    public SpriteAnimation(int imageWidth, int frameWidth, int frameHeight, double frameTime, int frameCount) {
        this(imageWidth, frameWidth, frameHeight, frameTime, frameCount, 0);
    }

    public void update(double deltaTime) {
        timeLeft += deltaTime;
        if (timeLeft >= frameDurations[currentFrameNum]) {
            timeLeft = 0;
            currentFrameNum++;
            if (currentFrameNum >= frameDurations.length) {
                currentFrameNum = 0;
            }
        }
    }

    public int getCurrentFrameNum() {
        return currentFrameNum;
    }

    public int[][] getFrameProps() {
        return frameProps;
    }
}
