package net.phoenixgi.greatnothing.world.entity;

import net.phoenixgi.greatnothing.Assets;
import net.phoenixgi.greatnothing.engine.graphics.brush.Brush;

public class BaseAnimationRenderer extends BaseEntityRenderer {
    public BaseAnimationRenderer(Entity entity) {
        super(entity);
    }

    @Override
    public void render(Brush brush) {
        Entity entity = getEntity();
        Sprite sprite = entity.getView().getSprite();
        EntityState state = entity.getState();
        if (getEntity().getState().isMoving()) {
            if (state.isMoveRight()) {
                sprite.setSpriteAnimation(Assets.greyMageStopRight);
            }
            if (state.isMoveLeft()) {
                sprite.setSpriteAnimation(Assets.greyMageStopLeft);
            }
            if (state.isMoveDown()) {
                sprite.setSpriteAnimation(Assets.greyMageStopDown);
            }
            if (state.isMoveUp()) {
                sprite.setSpriteAnimation(Assets.greyMageStopUp);
            }
        }
        super.render(brush);
    }
}
