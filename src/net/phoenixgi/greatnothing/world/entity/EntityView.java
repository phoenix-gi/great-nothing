package net.phoenixgi.greatnothing.world.entity;

public class EntityView {
    Sprite sprite;
    EntityRenderer renderer;

    private Entity entity;

    public EntityView(Entity entity) {
        this.entity = entity;
        renderer = new BaseEntityRenderer(entity);
    }

    public Entity getEntity() {
        return entity;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }

    public EntityRenderer getEntityRenderer() {
        return this.renderer;
    }

    public void setEntityRenderer(EntityRenderer renderer) {
        this.renderer = renderer;
    }
}
