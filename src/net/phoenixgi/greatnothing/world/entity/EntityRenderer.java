package net.phoenixgi.greatnothing.world.entity;

import net.phoenixgi.greatnothing.engine.graphics.brush.Brush;
import net.phoenixgi.greatnothing.world.entity.Entity;

public abstract class EntityRenderer {
    Entity entity;

    public EntityRenderer(Entity entity) {
        this.entity = entity;
    }

    public abstract void render(Brush brush);

    public Entity getEntity() {
        return entity;
    }
}
