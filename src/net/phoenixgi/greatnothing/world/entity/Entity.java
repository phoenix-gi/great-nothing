package net.phoenixgi.greatnothing.world.entity;

public class Entity {
    EntityState state;
    EntityView view;

    public Entity() {
        state = new EntityState(this);
        state.setHP(100);
        state.setMP(100);
        state.setMoney(0);
        state.setMoveSpeed(100);

        view = new EntityView(this);
    }

    public EntityState getState() {
        return state;
    }

    public EntityView getView() {
        return view;
    }
}
