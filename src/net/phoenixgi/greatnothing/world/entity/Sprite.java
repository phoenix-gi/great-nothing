package net.phoenixgi.greatnothing.world.entity;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Sprite {

    BufferedImage image;

    SpriteAnimation animation;

    public Sprite(BufferedImage image) {
        this.image = image;
        animation = new SpriteAnimation(image.getWidth(), image.getWidth(), image.getHeight(), 0, 1);
    }

    public Sprite(BufferedImage image, SpriteAnimation animation) {
        this(image);
        this.animation = animation;
    }

    public void draw(Graphics2D g, int x, int y) {
        int frameProps[][] = animation.getFrameProps();
        int frameProp[] = frameProps[animation.getCurrentFrameNum()];

        g.drawImage(image, x, y, x + frameProp[0], y + frameProp[1], frameProp[2], frameProp[3], frameProp[4], frameProp[5], null);
    }

    public SpriteAnimation getSpriteAnimation() {
        return animation;
    }

    public void setSpriteAnimation(SpriteAnimation animation) {
        this.animation = animation;
    }
}
