package net.phoenixgi.greatnothing.world.entity;

import net.phoenixgi.greatnothing.engine.graphics.brush.Brush;

import java.awt.*;

public class BaseEntityRenderer extends EntityRenderer {
    public BaseEntityRenderer(Entity entity) {
        super(entity);
    }

    @Override
    public void render(Brush brush) {
        Graphics2D g = (Graphics2D) brush.getRenderObject();
        Entity entity = getEntity();
        Sprite sprite = entity.getView().getSprite();
        sprite.draw(g, (int) entity.getState().getPosition().x, (int) entity.getState().getPosition().y);
    }
}
