package net.phoenixgi.greatnothing.world.entity;

import net.phoenixgi.greatnothing.world.Effect;
import net.phoenixgi.greatnothing.world.Item;
import net.phoenixgi.greatnothing.world.Vector2;

public class EntityState {
    int hp;
    int mp;
    int money;

    Item weapon;
    Item[] inventory;
    Effect[] effects;


    Vector2 position;
    Vector2 velocity;

    double moveSpeed;
    boolean moveUp, moveDown, moveRight, moveLeft;


    private Entity entity;

    public EntityState(Entity entity) {
        this.entity = entity;
    }

    public Entity getEntity() {
        return entity;
    }

    public Effect[] getEffects() {
        return effects;
    }

    public int getHP() {
        return hp;
    }

    public int getMP() {
        return mp;
    }

    public int getMoney() {
        return money;
    }

    public Item getWeapon() {
        return weapon;
    }

    public Item[] getInventory() {
        return inventory;
    }

    public Vector2 getPosition() {
        return position;
    }

    public Vector2 getVelocity() {
        return velocity;
    }


    public double getMoveSpeed() {
        return moveSpeed;
    }

    public void setEffects(Effect[] effects) {
        this.effects = effects;
    }

    public void setInventory(Item[] inventory) {
        this.inventory = inventory;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public void setHP(int hp) {
        this.hp = hp;
    }

    public void setMP(int mp) {
        this.mp = mp;
    }

    public void setWeapon(Item weapon) {
        this.weapon = weapon;
    }

    public void setMoveSpeed(double moveSpeed) {
        this.moveSpeed = moveSpeed;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public void setVelocity(Vector2 velocity) {
        this.velocity = velocity;
    }

    public void moveUp(boolean moveUp) {
        this.moveUp = moveUp;
    }

    public void moveDown(boolean moveDown) {
        this.moveDown = moveDown;
    }

    public void moveLeft(boolean moveLeft) {
        this.moveLeft = moveLeft;
    }

    public void moveRight(boolean moveRight) {
        this.moveRight = moveRight;
    }

    public boolean isMoveUp() {
        return moveUp;
    }

    public boolean isMoveDown() {
        return moveDown;
    }

    public boolean isMoveLeft() {
        return moveLeft;
    }

    public boolean isMoveRight() {
        return moveRight;
    }

    public boolean isMoving() {
        return moveUp || moveDown || moveLeft || moveRight;
    }
}
