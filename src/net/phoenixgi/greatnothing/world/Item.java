package net.phoenixgi.greatnothing.world;

import net.phoenixgi.greatnothing.world.entity.Sprite;

public class Item {
    private Sprite sprite;

    public Item() {

    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }
}
