package net.phoenixgi.greatnothing.world;

import net.phoenixgi.greatnothing.engine.graphics.brush.Brush;
import net.phoenixgi.greatnothing.world.entity.Entity;

public class Renderer {

    public static void render(World world, Brush brush) {
        for (Entity e : world.getEntities()) {
            e.getView().getEntityRenderer().render(brush);
        }
    }
}
