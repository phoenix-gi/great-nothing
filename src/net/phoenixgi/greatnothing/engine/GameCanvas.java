package net.phoenixgi.greatnothing.engine;

import net.phoenixgi.greatnothing.engine.input.GameInput;

import javax.swing.JComponent;

public abstract class GameCanvas {

    public abstract void render(GameScene scene);

    public abstract JComponent getJComponent();

    public abstract void changeInputListeners(GameInput gameInput);
}
