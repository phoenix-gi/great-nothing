package net.phoenixgi.greatnothing.engine.graphics.brush;

public abstract class Brush {
    public abstract Object getRenderObject();
}
