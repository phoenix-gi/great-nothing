package net.phoenixgi.greatnothing.engine;

import javax.swing.*;
import java.awt.*;

public class GameWindow {

    private JFrame frame;
    private GameCanvas canvas;

    public GameWindow(GameCanvas canvas, String title, int width, int height) {
        this.canvas = canvas;
        buildFrame(title, width, height);
    }

    private void buildFrame(String title, int width, int height) {
        frame = new JFrame(title);
        Toolkit tlk = Toolkit.getDefaultToolkit();
        final int frameWidth = width;
        final int frameHeight = height;
        final int screenWidth = tlk.getScreenSize().width;
        final int screenHeight = tlk.getScreenSize().height;
        final int x = (screenWidth - frameWidth) / 2;
        final int y = (screenHeight - frameHeight) / 2;
        frame.setBounds(x, y, frameWidth, frameHeight);
        frame.setResizable(false);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.add(canvas.getJComponent());
        frame.setFocusable(false);
    }

    public void showWindow() {
        frame.setVisible(true);
        canvas.getJComponent().grabFocus();
    }

    public GameCanvas getCanvas() {
        return canvas;
    }
}
