package net.phoenixgi.greatnothing.engine;

public class GameUpdater {
    GameWindow window;
    GameScene scene;

    public GameUpdater(GameWindow window) {
        this.window = window;
    }

    public void setScene(GameScene newScene) {
        this.scene = newScene;
        if (scene != null) {
            window.getCanvas().changeInputListeners(this.scene.getGameInput());
        }
    }

    public void start() {
        window.showWindow();
        double startTime = System.nanoTime();
        double deltaTime = 0;
        while (true) {
            try {
                Thread.sleep(10L);
                if (scene != null) {
                    scene.update(deltaTime / 1000000000.0);
                    window.getCanvas().render(scene);
                }
                double currentTime = System.nanoTime();
                deltaTime = currentTime - startTime;
                startTime = currentTime;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public GameWindow getWindow() {
        return window;
    }
}
