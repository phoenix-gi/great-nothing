package net.phoenixgi.greatnothing.engine;

import net.phoenixgi.greatnothing.engine.graphics.brush.Brush;
import net.phoenixgi.greatnothing.engine.input.GameInput;

public abstract class GameScene {

    protected GameUpdater updater;
    protected GameInput gameInput;

    public GameScene(GameUpdater updater) {
        this.updater = updater;
        createInput();
    }

    public abstract void update(double deltaTime);

    public abstract void render(Brush brush);

    public GameUpdater getGameUpdater() {
        return updater;
    }

    protected abstract void createInput();

    protected void setGameInput(GameInput gameInput) {
        this.gameInput = gameInput;
    }

    public GameInput getGameInput() {
        return gameInput;
    }
}
