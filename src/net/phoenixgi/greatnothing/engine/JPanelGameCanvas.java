package net.phoenixgi.greatnothing.engine;

import net.phoenixgi.greatnothing.engine.graphics.brush.Brush;
import net.phoenixgi.greatnothing.engine.graphics.brush.Graphics2DBrush;
import net.phoenixgi.greatnothing.engine.input.GameInput;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;

public class JPanelGameCanvas extends GameCanvas {

    JPanel panel;
    BufferedImage image;
    GameScene currentScene;

    public JPanelGameCanvas() {
        buildPanel();
    }

    private void buildPanel() {
        panel = new JPanel() {
            @Override
            public void paint(Graphics g) {
                int width = getWidth();
                int height = getHeight();
                image = (BufferedImage) createImage(width, height);
                java.awt.Graphics2D g2d = image.createGraphics();
                Brush brush = new Graphics2DBrush(g2d);
                if (currentScene != null) {
                    currentScene.render(brush);
                }
                g.drawImage(image, 0, 0, this);
            }
        };
    }

    @Override
    public void render(GameScene scene) {
        this.currentScene = scene;
        panel.repaint();
    }

    @Override
    public JComponent getJComponent() {
        return panel;
    }

    @Override
    public void changeInputListeners(GameInput gameInput) {
        if (currentScene != null) {
            panel.removeKeyListener((KeyListener) currentScene.getGameInput());
            panel.removeMouseListener((MouseListener) currentScene.getGameInput());
            panel.removeMouseWheelListener((MouseWheelListener) currentScene.getGameInput());
            panel.removeMouseMotionListener((MouseMotionListener) currentScene.getGameInput());
        }

        panel.addKeyListener((KeyListener) gameInput);
        panel.addMouseListener((MouseListener) gameInput);
        panel.addMouseWheelListener((MouseWheelListener) gameInput);
        panel.addMouseMotionListener((MouseMotionListener) gameInput);
    }
}
