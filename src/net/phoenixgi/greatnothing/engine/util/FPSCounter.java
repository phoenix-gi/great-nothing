package net.phoenixgi.greatnothing.engine.util;

public class FPSCounter {

    long startTime = System.nanoTime();
    int frames = 0;

    public void logFrame() {
        frames++;
        if (System.nanoTime() - startTime >= 1000000000) {
            System.out.println("FPS: " + frames);
            frames = 0;
            startTime = System.nanoTime();
        }
    }
}