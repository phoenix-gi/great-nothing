package net.phoenixgi.greatnothing.scenes;

import net.phoenixgi.greatnothing.Assets;
import net.phoenixgi.greatnothing.engine.GameScene;
import net.phoenixgi.greatnothing.engine.GameUpdater;
import net.phoenixgi.greatnothing.engine.graphics.brush.Brush;
import net.phoenixgi.greatnothing.inputs.MenuInput;

import java.awt.*;

public class MenuScene extends GameScene {
    public MenuScene(GameUpdater updater) {
        super(updater);
    }

    @Override
    public void update(double deltaTime) {
    }

    @Override
    public void render(Brush brush) {
        Graphics2D g = (Graphics2D) brush.getRenderObject();
        int width = getGameUpdater().getWindow().getCanvas().getJComponent().getWidth();
        int height = getGameUpdater().getWindow().getCanvas().getJComponent().getHeight();
        g.setColor(new Color(220, 232, 240));
        g.fillRect(0, 0, width, height);
        g.drawImage(Assets.logo, width / 2 - 320 / 2, height / 2 - 240 / 2 - 40, null);
        Font font = new Font("Arial", Font.BOLD, 16);
        g.setFont(font);
        g.setColor(Color.blue);
        FontMetrics metrics = g.getFontMetrics();
        String pressSpace = "press enter to start";
        g.drawString(pressSpace, width / 2 - metrics.stringWidth(pressSpace) / 2, height / 2 + 64);
    }

    @Override
    protected void createInput() {
        setGameInput(new MenuInput(this));
    }
}
