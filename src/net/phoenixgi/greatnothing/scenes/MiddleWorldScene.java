package net.phoenixgi.greatnothing.scenes;

import net.phoenixgi.greatnothing.Assets;
import net.phoenixgi.greatnothing.engine.GameScene;
import net.phoenixgi.greatnothing.engine.GameUpdater;
import net.phoenixgi.greatnothing.engine.graphics.brush.Brush;
import net.phoenixgi.greatnothing.inputs.MiddleWorldInput;
import net.phoenixgi.greatnothing.world.*;
import net.phoenixgi.greatnothing.world.entity.BaseAnimationRenderer;
import net.phoenixgi.greatnothing.world.entity.Entity;

import java.awt.*;

public class MiddleWorldScene extends GameScene {

    Entity hero;
    World world;

    public MiddleWorldScene(GameUpdater updater) {
        super(updater);
        hero = new Entity();
        hero.getState().setVelocity(new Vector2(0, 0));
        hero.getState().setPosition(new Vector2(0, 0));
        hero.getView().setSprite(Assets.greyMage);
        hero.getView().setEntityRenderer(new BaseAnimationRenderer(hero));
        world = new World();
        world.addEntity(hero);
    }

    @Override
    public void update(double deltaTime) {
        Updater.update(world, deltaTime);
    }

    @Override
    public void render(Brush brush) {
        Graphics2D g = (Graphics2D) brush.getRenderObject();
        int width = getGameUpdater().getWindow().getCanvas().getJComponent().getWidth();
        int height = getGameUpdater().getWindow().getCanvas().getJComponent().getHeight();
        g.translate(width / 2, height / 2);

        Renderer.render(world, brush);

        g.translate(-width / 2, -height / 2);
    }

    public Entity getHero() {
        return hero;
    }

    @Override
    protected void createInput() {
        MiddleWorldInput middleWorldInput = new MiddleWorldInput(this);
        setGameInput(middleWorldInput);
    }
}