package net.phoenixgi.greatnothing.scenes;

import net.phoenixgi.greatnothing.engine.GameScene;
import net.phoenixgi.greatnothing.engine.GameUpdater;
import net.phoenixgi.greatnothing.engine.graphics.brush.Brush;
import net.phoenixgi.greatnothing.engine.input.GameInput;
import net.phoenixgi.greatnothing.engine.util.FPSCounter;
import net.phoenixgi.greatnothing.inputs.TitleInput;

import java.awt.*;

public class TitleScene extends GameScene {
    FPSCounter fpsCounter = new FPSCounter();

    double timeLeft = 0;

    public TitleScene(GameUpdater updater) {
        super(updater);
    }

    public void setMenuScreen() {
        getGameUpdater().setScene(new MenuScene(getGameUpdater()));
    }

    @Override
    public void update(double deltaTime) {
        fpsCounter.logFrame();

        timeLeft += deltaTime;
        if (timeLeft >= 4) {
            setMenuScreen();
        }
    }

    @Override
    public void render(Brush brush) {
        Graphics2D g = (Graphics2D) brush.getRenderObject();
        int width = getGameUpdater().getWindow().getCanvas().getJComponent().getWidth();
        int height = getGameUpdater().getWindow().getCanvas().getJComponent().getHeight();
        g.setColor(Color.white);
        g.fillRect(0, 0, width, height);
        g.setColor(Color.black);
        Font font = new Font("Arial", Font.BOLD, 64);
        Font small = new Font("Arial", Font.ITALIC, 16);
        g.setFont(font);
        FontMetrics metrics = g.getFontMetrics();
        String author = "phoenix-gi";
        String special = "special for Ludum Dare 45";
        g.drawString(author, width / 2 - metrics.stringWidth(author) / 2, height / 2);
        g.setFont(small);
        metrics = g.getFontMetrics();
        g.setColor(new Color(155, 62, 6));
        g.drawString(special, width / 2 - metrics.stringWidth(special) / 2 + 64, height / 2 + 48);
    }

    @Override
    protected void createInput() {
        GameInput gameInput = new TitleInput(this);
        setGameInput(gameInput);
    }
}
